package kosuhina.tm.repository;

import kosuhina.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();

    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        tasks.add(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public List<Task> findByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public void clear() {
        tasks.clear();
    }

    public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size() -1) return null;
        return tasks.get(index);
    }

    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }


    public Task findById(final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task update(final Long id, final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setUserId(id);
        return task;
    }


    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if(!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }


    public List<Task> findAll() {
        return tasks;
    }
}
