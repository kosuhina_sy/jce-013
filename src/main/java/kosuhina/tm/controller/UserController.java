package kosuhina.tm.controller;

import kosuhina.tm.entity.User;
import kosuhina.tm.service.SessionService;
import kosuhina.tm.service.UserService;
import kosuhina.tm.util.HashUtil;

import java.util.List;

public class UserController extends AbstractController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

     public int loginUser(){
        System.out.println("[ВХОД В ПРИЛОЖЕНИЕ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН:");
        final String login = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ПАРОЛЬ:");
        final String passwordHash = HashUtil.md5(scanner.nextLine());
        final User user  = userService.findByLoginAndPassword(login, passwordHash);
        if (user == null) {
            System.out.println("[НЕВЕРНЫЙ ЛОГИН ИЛИ ПАРОЛЬ]");
            return 0;
        }
        SessionService sessionService = SessionService.getInstance();
        sessionService.setSessionId(user.getId());
        System.out.println("[OK]");
        return 0;
    }


    public int updateProfilePassword(){
        final Long id = SessionService.getInstance().getSessionId();
        if (id == null) {
            System.out.println("[ТЕКУЩИЙ ПОЛЬЗОВАТЕЛЬ НЕ ЗАДАН]");
            return 0;
        }
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ТЕКУЩИЙ ПАРОЛЬ:");
        final String passwordHash = HashUtil.md5(scanner.nextLine());
        final User user  = userService.findByIdAndPassword(id, passwordHash);
        if (user == null) {
            System.out.println("[НЕВЕРНЫЙ ПАРОЛЬ]");
            return 0;
        }
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ НОВЫЙ ПАРОЛЬ:");
        final String passwordHashNew = HashUtil.md5(scanner.nextLine());
        userService.updatePassword(id, passwordHash, passwordHashNew);
        System.out.println("[OK]");
        return 0;
    }

    public int logoutUser(){
        SessionService sessionService = SessionService.getInstance();
        sessionService.setSessionId(null);
        System.out.println("[ВАШ СЕАНС ЗАВЕРШЕН]");
        return 0;
    }

    public int createUser(){
        System.out.println("[СОЗДАНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН:");
        final String login = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ПАРОЛЬ:");
        final String password = scanner.nextLine();
        userService.create(login, password);
        System.out.println("[OK]");
        return 0;
    }

    public int listUser(){
        System.out.println("[СПИСОК ПОЛЬЗОВАТЕЛЕЙ]");
        int index = 1;
        viewUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewUsers(final List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (final User user: users) {
            System.out.println(index + ". id:" + user.getId() + " Логин: " + user.getLogin());
            index++;
        }
    }

    public int clearUser(){
        System.out.println("[УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЕЙ]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserById(){
        System.out.println("[ИЗМЕНЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        final Long id = Long.parseLong(scanner.nextLine());
        final User user  = userService.findById(id);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        updateUserName(user);
        return 0;
    }

    public int updateUserByIndex(){
        System.out.println("[ИЗМЕНЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПОЛЬЗОВАТЕЛЯ:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final User user  = userService.findByIndex(index);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        updateUserName(user);
        return 0;
    }

    public int updateUserByLogin(){
        System.out.println("[ИЗМЕНЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН ПОЛЬЗОВАТЕЛЯ:");
        final String login = scanner.nextLine();
        final User user  = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        updateUserName(user);
        return 0;
    }

    public int updateUserProfile(){
        System.out.println("[ИЗМЕНЕНИЕ ТЕКУЩЕГО ПОЛЬЗОВАТЕЛЯ]");
        final Long id = SessionService.getInstance().getSessionId();
        if (id == null) {
            System.out.println("[ТЕКУЩИЙ ПОЛЬЗОВАТЕЛЬ НЕ ЗАДАН]");
            return 0;
        }
        final User user  = userService.findById(id);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        updateUserName(user);
        return 0;
    }

    public void updateUserName(User user){
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ФАМИЛИЮ ПОЛЬЗОВАТЕЛЯ:");
        final String firstName = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИМЯ ПОЛЬЗОВАТЕЛЯ:");
        final String lastName = scanner.nextLine();
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ОТЧЕСТВО ПОЛЬЗОВАТЕЛЯ:");
        final String middleName = scanner.nextLine();
        userService.update(user.getId(), firstName, lastName, middleName);
        System.out.println("[OK]");
    }

    public int removeUserByIndex(){
        System.out.println("[УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПОЛЬЗОВАТЕЛЯ:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final User user  = userService.removeByIndex(index);
        if (user == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeUserById(){
        System.out.println("[УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        final long id = scanner.nextLong();
        final User user  = userService.removeById(id);
        if (user == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin(){
        System.out.println("[УДАЛЕНИЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН ПОЛЬЗОВАТЕЛЯ:");
        final String login = scanner.nextLine();
        final User user  = userService.removeByLogin(login);
        if (user == null) System.out.println("[ОШИБКА]");
        else System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[ДАННЫЕ ПОЛЬЗОВАТЕЛЯ]");
        System.out.println("ID: " + user.getId());
        System.out.println("ЛОГИН: " + user.getLogin());
        System.out.println("ФАМИЛИЯ: " + user.getFirstName());
        System.out.println("ИМЯ: " + user.getLastName());
        System.out.println("ОТЧЕСТВО: " + user.getMiddleName());
        System.out.println("[OK]");
    }

    public int viewUserByIndex() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ИНДЕКС ПОЛЬЗОВАТЕЛЯ:");
        final int index = scanner.nextInt() - 1;
        final User user = userService.findByIndex(index);
        viewUser(user);
        return 0;
    }

    public int viewUserById() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        viewUser(user);
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("ПОЖАЛУЙСТА, ВВЕДИТЕ ЛОГИН ПОЛЬЗОВАТЕЛЯ:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }

    public int viewUserProfile() {
        System.out.println("ПРОФИЛЬ ТЕКУЩЕГО ПОЛЬЗОВАТЕЛЯ:");
        final Long id = SessionService.getInstance().getSessionId();
        if (id == null) {
            System.out.println("[ТЕКУЩИЙ ПОЛЬЗОВАТЕЛЬ НЕ ЗАДАН]");
            return 0;
        }
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[ОШИБКА]");
            return 0;
        }
        viewUser(user);
        return 0;
    }


}
