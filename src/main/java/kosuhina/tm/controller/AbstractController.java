package kosuhina.tm.controller;

import java.util.Scanner;

/**
 * Вынесен сканнер для возможности наследования контроллерами Project и Task
 */
public class AbstractController {

    protected final Scanner scanner = new Scanner(System.in);

}
