package kosuhina.tm.service;

import kosuhina.tm.entity.Project;
import kosuhina.tm.entity.Task;
import kosuhina.tm.repository.ProjectRepository;

import java.util.List;

/**
 * Сервисный класс для работы с репозиторием проектов
 */

public class ProjectService {

    private final ProjectRepository projectRepository;

    /**
     * Конструктор класса
     *
     * @param projectRepository репозиторий проектов
     */

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Создание проекта и добавление его в список проектов
     *
     * @param name имя проекта
     * @return
     */

    public Project create(String name, Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    /**
     * Очистка списка проектов
     */
    public void clear() {
        projectRepository.clear();
    }

    /**
     * Поиск проекта по индексу
     *
     * @param index индекс проекта
     * @return проект
     */
    public Project findByIndex(int index) {
        return projectRepository.findByIndex(index);
    }

    /**
     * Удаление проекта по индексу
     *
     * @param index индекс проекта
     * @return удаленный проект
     */

    public Project removeByIndex(int index) {
        return projectRepository.removeByIndex(index);
    }

    /**
     * Удаление проекта по коду
     *
     * @param id код проекта
     * @return удаленный проект
     */

    public Project removeById(Long id) {
        return projectRepository.removeById(id);
    }

    /**
     * Удаление проекта по имени
     *
     * @param name имя проекта
     * @return удаленный проект
     */

    public Project removeByName(String name) {
        return projectRepository.removeByName(name);
    }

    /**
     * Поиск проекта по коду
     *
     * @param id код проекта
     * @return проект
     */

    public Project findByIndexd(int index) {
        return projectRepository.findByIndex(index);
    }


    /**
     * @return
     */

    public List<Project> findAll() {return projectRepository.findAll();}

    public List<Project> findAllByUserId(Long userId) {
        if(userId == null) return null;
        return projectRepository.findAllByUserId(userId);
    }


}
