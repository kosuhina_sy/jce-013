package kosuhina.tm.service;

import kosuhina.tm.entity.User;
import kosuhina.tm.enumerated.Role;
import kosuhina.tm.repository.TaskRepository;
import kosuhina.tm.repository.UserRepository;
import kosuhina.tm.util.HashUtil;

import java.util.List;

public class UserService {

    private final UserRepository userRepository;

    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(final String login, final String password, final Role role){
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    public User updatePassword(final Long id, final String passwordHash,final String passwordHashNew) {
        final User user = findByIdAndPassword(id, passwordHash);
        if (user == null) return null;
        user.setPasswordHash(passwordHashNew);
        return user;
    }

    public User update(final Long id, final String firstName, final String lastName, final String middleName) {
        final User user = findById(id);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

     public User findByIndex(final Integer index) {
        if(index == null) return null;
        return userRepository.findByIndex(index);
    }

    /**
     * Создание пользователя по логину и паролю
     */
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        final String passwordHash = HashUtil.md5(password);
        if (existsByLogin(login)) return null;  // если уже есть пользователь с таким логином, его не создавать
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        userRepository.add(user);
        return user;
    }

    public User add(final User user) {
        if (user == null) return null;
        return userRepository.add(user);
    }

    public List<User> findAll() { return userRepository.findAll();  }

    public User findById(final Long id) {
        if(id == null) return null;
        return userRepository.findById(id);
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public User findByLoginAndPassword(final String login, final String passwordHash) {
        if (login == null || login.isEmpty()) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.findByLoginAndPassword(login, passwordHash);
    }

    public User findByIdAndPassword(final Long id, final String passwordHash) {
        if (id == null) return null;
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.findByIdAndPassword(id, passwordHash);
    }

    public User removeByIndex(final Integer index) {
        if (index == null) return null;
        return userRepository.removeByIndex(index);
    }
    public User removeById(final Long id) {
        if(id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    public void clear() { userRepository.clear(); }

    public boolean existsByLogin(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.existsByLogin(login);
    }

}
