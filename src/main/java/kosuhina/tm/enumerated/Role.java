package kosuhina.tm.enumerated;

public enum  Role {

    USER("Пользователь"),
    ADMIN("Администратор");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override //переопределение метода toString
    public String toString() {
        return displayName;
    }

    public static void main(String[] args) {
        System.out.println(Role.USER.name());
        System.out.println(Role.USER.getDisplayName());
        System.out.println(Role.USER);
    }
}
